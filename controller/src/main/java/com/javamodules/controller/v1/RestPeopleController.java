package com.javamodules.controller.v1;

import com.javamodules.service.PeopleService;
import com.javamodules.service.model.People;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1")
@AllArgsConstructor
public class RestPeopleController {

    private final PeopleService peopleService;

    @GetMapping("/people/{id}")
    public People getPeople(@PathVariable Integer id) {
        return peopleService.getPeople(id);
    }

}

