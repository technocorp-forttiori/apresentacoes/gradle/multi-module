package com.javamodules.service;

import com.javamodules.service.model.People;
import org.springframework.stereotype.Service;

@Service
public class PeopleService {

    public People getPeople(Integer id) {
        if (id == 1) {
            return People.builder()
                    .name("Fernanda")
                    .cpf("123456789")
                    .build();
        } else{

            throw new RuntimeException("Pessoa não encontrada");
        }
    }
}
